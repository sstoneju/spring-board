<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- jstl 코어 태그 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- jstl 시간 출력 태그 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="org.springframework.web.util.UrlPathHelper" %>
<!-- context 경로 -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<div id = "header">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script language="JavaScript">

    // 쿠키 생성
    function setCookie(cName, cValue, cDay){
        var expire = new Date();
        expire.setDate(expire.getDate() + cDay);
        cookies = cName + '=' + escape(cValue) + '; path=/ '; // 한글 깨짐을 막기위해 escape(cValue)를 합니다.
        if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
        document.cookie = cookies;
    }
 
    // 쿠키 가져오기
    function getCookie(cName) {
        cName = cName + '=';
        var cookieData = document.cookie;
        var start = cookieData.indexOf(cName);
        var cValue = '';
        if(start != -1){
            start += cName.length;
            var end = cookieData.indexOf(';', start);
            if(end == -1)end = cookieData.length;
            cValue = cookieData.substring(start, end);
        }
        return unescape(cValue);
    }

	</script>
	<%
	UrlPathHelper urlPathHelper = new UrlPathHelper();
	String originalURL = urlPathHelper.getOriginatingRequestUri(request);
	out.print("이곳의 주소: "+originalURL);
	%>


	<h1>LALA STONEJU PAGE</h1>
	
    <c:if test="${msg == 'success'}">
    	<h2>${sessionScope.name}(${sessionScope.id})님 환영합니다.</h2>
    </c:if>
    
    <h1><a href = "${path }/board/list.do"> 게시판 입쟝!</a></h1>
    
    <input type="button" value="쿠키 생성" onclick="setCookie('${pageContext.request.requestURI}', '<%= request.getRequestURL() %>', 1)">
	<input type="button" value="쿠키 보기" onclick="alert(getCookie('${pageContext.request.requestURI}'))">
	<input type="button" value="쿠키 삭제" onclick="setCookie('${pageContext.request.requestURI}', '', -1)">
	    
    
</div>
