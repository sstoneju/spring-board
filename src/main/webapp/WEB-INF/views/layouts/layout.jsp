<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>
	<div id="wrap">
      <section id="menu">
          <tiles:insertAttribute name="menu" />
      </section>
    	<!-- container -->
    	<div id="container">
    	
	      <header id="header">
	          <tiles:insertAttribute name="header" />
	      </header>
	           
	      <section id="site-content">
	          <tiles:insertAttribute name="content" />
	      </section>
    	</div><!-- //container -->
       
      <footer id="footer">
          <tiles:insertAttribute name="footer" />
      </footer>
</div> 
</body>
</html>