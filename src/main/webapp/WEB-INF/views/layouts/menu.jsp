<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id = "menu">

<ul>
	<li><a href="${path}/board/list.do">게시판</a></li>
	<li><a href="${path }/shop/product/list.do	">쇼핑몰</a></li>
	<li>
		<c:choose>
	    <c:when test="${sessionScope.id == null}">
	        <a href="${path}/member/login.do">로그인</a>
	    </c:when>
	    <c:otherwise>
	        ${sessionScope.name}님이 로그인중입니다.
	        <a href="${path}/member/logout.do">로그아웃</a>
	    </c:otherwise>
	</c:choose>
	</li>
	<li> </li>
	
</ul>

</div>